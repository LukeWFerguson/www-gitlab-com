---
layout: markdown_page
title: "Director Notes"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Common Links

| **Workflow** | [**How may we be of service?**](production/#workflow--how-we-work) | **GitLab.com Status** | [**`STATUS`**](https://status.gitlab.com/)
| **Issue Trackers** | [**Infrastructure**](https://gitlab.com/gitlab-com/infrastructure/issues/): [Milestones](https://gitlab.com/gitlab-com/infrastructure/milestones), [OnCall](https://gitlab.com/gitlab-com/infrastructure/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=oncall) | [**Production**](https://gitlab.com/gitlab-com/production/issues/): [Incidents](https://gitlab.com/gitlab-com/production/issues?label_name%5B%5D=incident), [Changes](https://gitlab.com/gitlab-com/production/issues?label_name%5B%5D=change), [Deltas](https://gitlab.com/gitlab-com/production/issues?label_name%5B%5D=delta)  | [**Delivery**](https://gitlab.com/gitlab-org/release/framework)
| **Slack Channels** | [#infrastructure-lounge](https://gitlab.slack.com/archives/infrastructure-lounge), [#database](https://gitlab.slack.com/archives/database) | [#alerts](https://gitlab.slack.com/archives/alerts), [#production](https://gitlab.slack.com/archives/production) | [#g_delivery](https://gitlab.slack.com/archives/g_delivery)
| **Operations** | [Runbooks](https://gitlab.com/gitlab-com/runbooks) (please contribute!) | **On-call**: [Handover Document](https://docs.google.com/document/d/1IrTi06fUMgxqDCDRD4-e7SJNPvxhFML22jf-3pdz_TI), [Reports](https://gitlab.com/gitlab-com/infrastructure/issues?scope=all&utf8=%E2%9C%93&state=closed&label_name[]=oncall%20report) |

## Workload Workflow

### OKRs in GitLab

OKRs outline our quarterly objectives, and, by implication, our priorities. Each OKR for Infrastructure is represented as an **epic** in GitLab in the [_GitLab Infrastructure Team_ ](https://gitlab.com/gitlab-com/gl-infra), as is each Key Result. Additionally, each team also has OKRs and key result epics, linked as follows:

![](img/Workload-1.0.0.png)

Thus, epics are linked to create a tree. An OKR epic will contain links to its KRs and links to OKRs for each o the teams.

* `Infrastructure`: https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/33
      * `Reliability.AS`: https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/37`
      * `Reliability.DS`: https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/38
      * `Reliability.JF`: https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/39

Each of those OKRs have their corresponding KRs linked to them, which are also represented as epics. The KR epics have their corresponding projects' epics linked to them. Project epics contain issues relevant to said project.

#### Milestones

Milestones are used as time-boxes to orkganize work on two-week sprints.